# ExperisNoroffTask24

Part 2 -Task 24

• Create a new ASP.NetCore MVC solution/application

• It must have a new home page

• Use the viewbag to display the current time in the home page

• It must have a page to display information about supervisors(first just one)

• Upgrade the page to display a list of supervisors

• Do not use any previously created views/layouts or action methods that comes with the solution
