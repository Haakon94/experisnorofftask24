using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers {
    public class HomeController : Controller {
        public IActionResult Index() {
            ViewBag.Message = DateTime.Now.ToString();
            return View("MyFirstView");
        }

        public IActionResult SupervisorInfo() {

            // Create two supervisors and then add them to a supervisorlist and return the list into view.
            Supervisor supervisor = new Supervisor {
                Id = 1,
                Name = "Per Hansen"
            };
            Supervisor supervisor2 = new Supervisor {
                Id = 2,
                Name = "Jan Jansen"
            };
            List<Supervisor> supervisorList = new List<Supervisor>();
            supervisorList.Add(supervisor);
            supervisorList.Add(supervisor2);


            return View(supervisorList);
        }
    }
}
