﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models {
    public class Supervisor {

        public int Id { get; set; }

        public string Name { get; set; }

    }
}
